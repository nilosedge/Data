package net.nilosplace.Data;

import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.ConcurrentLinkedQueue;

import net.nilosplace.Data.models.Candle;
import net.nilosplace.Data.models.Order;
import net.nilosplace.Data.models.Trader;
import net.nilosplace.Data.models.Order.Type;
import net.nilosplace.Data.util.LimitedQueue;
import net.nilosplace.Data.util.ModelMapper;

public class TradeProcessThread extends Thread {

	private ModelMapper m = new ModelMapper();
	private ConcurrentLinkedQueue<Trader> traders;
	private ArrayList<Candle> candles;
	private int[] keys;
	private boolean terminate = false;
	private LimitedQueue<Integer> time = new LimitedQueue<Integer>(100);
	
	public TradeProcessThread(ConcurrentLinkedQueue<Trader> traders) {
		this.traders = traders;
	}

	public void run() {
		System.out.println("Starting up Trade Processor thread");
		ArrayList<Trader> list = new ArrayList<Trader>();
		
		while(!terminate) {
			try {
				Trader t;
				for(int c = 0; c < 50 && (t = traders.poll()) != null; c++) {
					list.add(t);
				}
				for(Trader tr: list) {
					calculateProfit(tr);
				}
				if(list.size() == 0) {
					sleep(100);
				}
				list.clear();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Trade Processor thread Shuting down");
	}
	
	private void calculateProfit(Trader t) {
		//Date start = new Date();
		//Date end = new Date();
		
		Order o = t.getOrder();
		int traderShapeId = t.getShapeId();
		Type orderType = o.getType();
		int stopLossPips = o.getStoplosspips();
		int stopProfitPips = o.getStopprofitpips();
		int highdiff = 0;
		int lowdiff = 0;
		//start = new Date();
		for(int j = 0; j < candles.size(); j++) {
			
			if(keys[j] == traderShapeId) {

				int k = j + 1;
				
				while(k < candles.size()) {

					highdiff = (int)((candles.get(k).getHigh() - candles.get(j).getClose()) * 100);
					lowdiff = (int)((candles.get(j).getClose() - candles.get(k).getLow()) * 100);

					if(orderType == Order.Type.BUY) {
						if(highdiff >= stopProfitPips && lowdiff >= stopLossPips) {
							break;
						} else if(highdiff >= stopProfitPips) {
							t.profit(stopProfitPips);
							break;
						} else if(lowdiff >= stopLossPips) {
							t.loss(stopLossPips);
							break;
						}
					} else if(orderType == Order.Type.SELL) {
						if(highdiff >= stopLossPips && lowdiff >= stopProfitPips) {
							break;
						} else if(highdiff >= stopLossPips) {
							t.loss(stopLossPips);
							break;
						} else if(lowdiff >= stopProfitPips) {
							t.profit(stopProfitPips);
							break;
						}
					}
					k++;
				}
				j = k;
				
			}
		}
		//end = new Date();
		//time.add((int)(end.getTime() - start.getTime()));
	}

	public int getAvg() {
		long ret = 0;
		for(long l: time) {
			ret += l;
		}
		return (int)(ret / time.getLimit());
	}

	public ArrayList<Candle> getCandles() {
		return candles;
	}
	public void setCandles(ArrayList<Candle> candles) {
		this.candles = candles;
		keys = new int[candles.size()];
		for(int i = 0; i < keys.length; i++) {
			keys[i] = candles.get(i).getShape().getIdInt();
		}
	}
}
