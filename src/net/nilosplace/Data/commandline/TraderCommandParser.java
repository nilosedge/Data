package net.nilosplace.Data.commandline;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.concurrent.ConcurrentLinkedQueue;

import net.nilosplace.Data.TradeProcessThread;
import net.nilosplace.Data.handlers.CandleData;
import net.nilosplace.Data.handlers.Population;
import net.nilosplace.Data.models.Candle;
import net.nilosplace.Data.models.Trader;
import net.nilosplace.Data.programs.CommandInterface;


public class TraderCommandParser extends CommandParser implements CommandInterface {

	private int threadCount = 1;
	private ConcurrentLinkedQueue<Trader> processTraderQueue = new ConcurrentLinkedQueue<Trader>();
	TradeProcessThread[] threads;
	
	public TraderCommandParser(Population pop, CandleData data) {
		super(pop, data);
		String count = System.getProperty("threadCount");
		int tc = 0;
		try {
			tc = Integer.parseInt(count);
		} catch (Exception e) {
			tc = threadCount;
		}
		threadCount = tc;
		threads = new TradeProcessThread[threadCount];

		for(int i = 0; i < threadCount; i++) {
			threads[i] = new TradeProcessThread(processTraderQueue);
			threads[i].start();
		}
	}

	public void run(LinkedList<String> list) {
		
		String command = list.remove(0);
		if(command != null) {
			if(command.equals("load")) {
				parseLoad(list);
			} else if(command.equals("save")) {
				parseSave(list);
			} else if(command.equals("prune")) {
				pruneTraders(list);
			} else if(command.equals("stats")) {
				parseStats(list);
			} else if(command.equals("process")) {
				parseProcess(list);
			} else if(command.equals("breed")) {
				parseBreed(list);
			} else if(command.equals("reset")) {
				pop.resetTraders();
			} else if(command.equals("clear")) {
				System.out.println("Clearing: " + pop.getTraderList().size() + " traders.");
				pop.clearTraders();
			} else if(command.equals("gen")) {
				parseGen(list);
			} else if(command.equals("help")) {
				printHelp();
			} else {
				printError("trader");
			}
		} else {
			printError("trader");
		}
	}

	private void parseProcess(LinkedList<String> list) {
		String key = list.poll();
		if(key != null) {
			processTraders(Integer.parseInt(key));
		} else {
			processTraders();
		}
	}
	
	private void processTraders(Integer key) {
		pop.resetTraders(key);
		ArrayList<Trader> traders = pop.getTraders(key);
		processTraders(traders);
	}

	private void processTraders() {
		pop.resetTraders();
		ArrayList<Trader> traders = pop.getTraderList();
		processTraders(traders);
	}
		
	private void processTraders(ArrayList<Trader> traders) {
		if(traders.size() > 0) {
			setCandles();
			for(Trader t: traders) {
				processTraderQueue.add(t);
			}
			while(processTraderQueue.size() > 0) {
				try {
					System.out.println("Waiting for Queue to finish: " + processTraderQueue.size());
					int runningTime = 0;
					for(int i = 0; i < threads.length; i++) {
						runningTime += threads[i].getAvg();
					}
					System.out.println("Avg Running time: " + (runningTime / threads.length));
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		} else {
			System.out.println("No Traders loaded please load or generate traders");
		}
	}

	private void setCandles() {
		for(TradeProcessThread t: threads) {
			t.setCandles(data.getCandles());
		}
	}

	private void parseGen(LinkedList<String> list) {
		String key = list.poll();
		if(key != null) {
			pop.addShape(Integer.parseInt(key));
		} else {
			genTraders();
		}
	}

	private void genTraders() {
		if(data.getCandles().size() > 1) {
			for(Candle c: data.getCandles()) {
				pop.addShape(c.getShape().getIdInt());
			}
		} else {
			System.out.println("Error: Please load data first.");
		}
	}

	private void parseBreed(LinkedList<String> list) {
		String key = list.poll();
		if(key != null) {
			pop.breedTraders(Integer.parseInt(key));
		} else {
			pop.breedTraders();
		}
	}

	private void parseStats(LinkedList<String> list) {
		String key = list.poll();
		if(key != null) {
			pop.printTraderStats(Integer.parseInt(key));
		} else {
			System.out.println("Traders: " + pop.getTraderList().size());
			pop.printTraderStats();
		}
	}


	private void pruneTraders(LinkedList<String> list) {
		String key = list.poll();
		if(key != null) {
			pop.pruneTraders(Integer.parseInt(key));
		} else {
			pop.pruneTraders();
		}
	}
	
	private void parseLoad(LinkedList<String> list) {
		String fileName = list.poll();
		if(fileName != null) {
			pop.loadTraders("datadir/" + fileName);
		} else {
			printError("trader");
		}
	}
	
	private void parseSave(LinkedList<String> list) {
		String fileName = list.poll();
		if(fileName != null) {
			pop.saveTraders("datadir/" + fileName);
		} else {
			printError("trader");
		}
	}

	private void printHelp() {
		System.out.println("trader load filename.data");
		System.out.println("trader save filename.data");
		System.out.println("trader stats");
		System.out.println("trader gen");
		System.out.println("trader reset");
		System.out.println("trader print");
		System.out.println("trader process");
		System.out.println("trader breed");
		System.out.println("trader clear");
		System.out.println("trader eval");
		System.out.println("trader help");
	}
}
