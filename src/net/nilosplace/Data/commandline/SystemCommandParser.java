package net.nilosplace.Data.commandline;

import java.util.LinkedList;

import net.nilosplace.Data.handlers.CandleData;
import net.nilosplace.Data.handlers.Population;
import net.nilosplace.Data.programs.CommandInterface;

public class SystemCommandParser extends CommandParser implements CommandInterface {

	public SystemCommandParser(Population pop, CandleData data) {
		super(pop, data);
	}

	@Override
	public void run(LinkedList<String> list) {

		String command = list.poll();
		
		if(command != null) {
			if(command.equals("gc")) {
				System.gc();
			} else if(command.equals("stats")) {
				System.out.println("Candles Size: " + data.getCandles().size());
				System.out.println("Traders: " + pop.getTraderList().size());
			} else if(command.equals("exit") || command.equals("quit")) {
				System.exit(0);
			} else if(command.equals("help")) {
				printHelp();
			} else {
				System.out.println("Error: didn't understand command: " + command);
				printHelp();
			}
		}
	}

	private void printHelp() {
		System.out.println("Help: ");
		System.out.println("trader");
		System.out.println("candle");
		
		System.out.println("stats");
		System.out.println("gc");
		System.out.println("quit or exit");
	}
}
