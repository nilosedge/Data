package net.nilosplace.Data.commandline;

import net.nilosplace.Data.handlers.CandleData;
import net.nilosplace.Data.handlers.Population;

public class CommandParser {

	protected Population pop;
	protected CandleData data;
	
	public CommandParser(Population pop, CandleData data) {
		this.pop = pop;
		this.data = data;
	}

	protected void printError(String command) {
		System.out.println("Error: Please type \"" + command + " help\" to learn this command");
	}
}
