package net.nilosplace.Data.commandline;

import java.util.LinkedList;

import net.nilosplace.Data.handlers.CandleData;
import net.nilosplace.Data.handlers.Population;
import net.nilosplace.Data.programs.CommandInterface;

public class CandleCommandParser extends CommandParser implements CommandInterface {

	public CandleCommandParser(Population pop, CandleData data) {
		super(pop, data);
	}

	public void run(LinkedList<String> list) {

		String command = list.remove(0);
		if(command != null) {
			if(command.equals("load")) {
				parseLoad(list);
			} else if(command.equals("stats")) {
				data.candleStats();
			} else if(command.equals("clear")) {
				System.out.println("Clearing: " + data.getCandles().size() + " candles.");
				data.clearCandles();
			} else if(command.equals("help")) {
				System.out.println("load filename.data");
				System.out.println("stats");
				System.out.println("clear");
				System.out.println("help");
			} else {
				System.out.println("Unknown command: " + command);
				printError("candle");
			}
		} else {
			printError("candle");
		}
	}

	private void parseLoad(LinkedList<String> list) {
		String fileName = list.poll();
		if(fileName != null) {
			System.out.println("Loading Candles from: " + fileName);
			data.loadData("datadir/" + fileName);
		} else {
			printError("candle");
		}
		
	}
	

}
