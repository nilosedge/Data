package net.nilosplace.Data.programs;

import java.util.LinkedList;

public class CommandProgram {

	protected CommandInterface iface;
	
	public CommandProgram(CommandInterface iface) {
		this.iface = iface;
	}

	protected void processCommand(String s) {
		LinkedList<String> list = new LinkedList<String>();
		for(String s1: s.split(" ")) {
			list.add(s1);
		}
		iface.run(list);
	}
}
