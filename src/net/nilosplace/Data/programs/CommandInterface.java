package net.nilosplace.Data.programs;

import java.util.LinkedList;

public interface CommandInterface {

	public void run(LinkedList<String> list);
}
