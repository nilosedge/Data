package net.nilosplace.Data.programs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class CommandLineProgram extends CommandProgram {

	public CommandLineProgram(CommandInterface iface) throws IOException {
		super(iface);

		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String s = null;
		
		processCommand("candle load DAT_ASCII_AUDJPY_M1_2013.csv");
		//processCommand("trader load file_2013_gen1");
		
		System.out.println("Please enter commands: ");
		System.out.print("> ");
		while((s = reader.readLine()) != null) {
			processCommand(s);
			System.out.print("> ");
		}
	}
	

}
