package net.nilosplace.Data;

import net.nilosplace.Data.handlers.PopulationHandler;
import net.nilosplace.Data.programs.CustomProgram;

public class Main {
	
	PopulationHandler ph = new PopulationHandler();

	public static void main(String[] args) throws Exception {
		new Main();
	}
	
	public Main() throws Exception {
		//CandleShape shape = new CandleShape(18, 93, 5);
		//System.out.println(shape.getIdInt());
		//new CommandLineProgram(ph);
		new CustomProgram(ph);
	}

}