package net.nilosplace.Data.handlers;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

import net.nilosplace.Data.models.Candle;

public class CandleData {
	private ArrayList<Candle> candles = new ArrayList<Candle>();

	public void loadData(String fileName) {
		candles.clear();
		try {
			BufferedReader reader = new BufferedReader(new FileReader(fileName));
			String line = null;
			while ((line = reader.readLine()) != null) {
				String[] s = line.split(";");
				Candle c = new Candle(Double.parseDouble(s[1]), Double.parseDouble(s[2]), Double.parseDouble(s[3]), Double.parseDouble(s[4])); 
				candles.add(c);
			}
			reader.close();
			candleStats();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Error: Please check file format: " + e.getMessage());
			System.out.println("Lookup for: 20140101 170000;93.517000;93.538000;93.516000;93.525000;0\n");
		}
	}
	
	public void clearCandles() {
		candles.clear();
	}

	public ArrayList<Candle> getCandles() {
		return candles;
	}
	public void setCandles(ArrayList<Candle> candles) {
		this.candles = candles;
	}

	public void candleStats() {
		System.out.println("Candles Loaded: " + candles.size());
		
	}
}

