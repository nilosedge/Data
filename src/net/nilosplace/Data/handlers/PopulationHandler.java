package net.nilosplace.Data.handlers;

import java.util.LinkedList;

import net.nilosplace.Data.commandline.CandleCommandParser;
import net.nilosplace.Data.commandline.SystemCommandParser;
import net.nilosplace.Data.commandline.TraderCommandParser;
import net.nilosplace.Data.programs.CommandInterface;

public class PopulationHandler implements CommandInterface {

	private Population pop = new Population();
	private CandleData data = new CandleData();

	private TraderCommandParser traderCommand = new TraderCommandParser(pop, data);
	private CandleCommandParser candleCommand = new CandleCommandParser(pop, data);
	private SystemCommandParser systemCommand = new SystemCommandParser(pop, data);

	public void run(LinkedList<String> list) {
		String command = list.remove(0);
		if(command.equals("candle")) {
			candleCommand.run(list);
		} else if(command.equals("trader")) {
			traderCommand.run(list);
		} else {
			list.add(0, command);
			systemCommand.run(list);
		}
	}
}
