package net.nilosplace.Data.handlers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import net.nilosplace.Data.models.Trader;
import net.nilosplace.Data.util.ModelMapper;

import org.codehaus.jackson.annotate.JsonIgnore;

public class Population {
	
	private HashMap<Integer, ArrayList<Trader>> map = new HashMap<Integer, ArrayList<Trader>>();
	private ModelMapper m = new ModelMapper();
	
	public void addShape(Integer key) {
		if(!map.containsKey(key) || map.get(key).size() == 0) {
			ArrayList<Trader> list = new ArrayList<Trader>();
			list.add(new Trader(key));
			list.add(new Trader(key));
			map.put(key, list);
		}
	}
	
	public HashMap<Integer, ArrayList<Trader>> getMap() {
		return map;
	}
	public void setMap(HashMap<Integer, ArrayList<Trader>> map) {
		this.map = map;
	}
	
	@JsonIgnore
	public ArrayList<Trader> getTraders(Integer key) {
		return map.get(key);
	}

	@JsonIgnore
	public ArrayList<Trader> getTraderList() {
		ArrayList<Trader> ret = new ArrayList<Trader>();

		for(int key: map.keySet()) {
			ArrayList<Trader> traders = map.get(key);
			for(Trader t: traders) {
				ret.add(t);
			}
		}
		return ret;
	}
	
	@JsonIgnore
	public void breedTraders() {
		for(int key: map.keySet()) {
			breedTraders(key);
		}
	}
	
	@JsonIgnore
	public void breedTraders(Integer key) {
		breedTraders(map.get(key));
	}
	
	private void breedTraders(ArrayList<Trader> list) {
		ArrayList<Trader> children = new ArrayList<Trader>();
		if(list.size() > 1) {
			for(int i = 0; i < list.size(); i++) {
				for(int j = 0; j < list.size(); j++) {
					children.addAll(breedTraders(list.get(i), list.get(j)));
				}
			}
		} else if(list.size() == 1) {
			children.addAll(breedTraders(list.get(0)));
		}
		list.addAll(children);
	}

	private ArrayList<Trader> breedTraders(Trader t1, Trader t2) {

		ArrayList<Trader> list = new ArrayList<Trader>();
		Trader c1 = new Trader(t1.getShapeId());
		Trader c2 = new Trader(t2.getShapeId());
		
		double rand = Math.random();
		if(rand > .875) {
			c1.setStoplosspips(t1.getStoplosspips());
			c1.setStopprofitpips(t1.getStoplosspips());
			c2.setStoplosspips(t1.getStoplosspips());
			c2.setStopprofitpips(t1.getStopprofitpips());
			
			c1.setType(t2.getType());
			c2.setType(t1.getType());
		} else if(rand > .75) {
			c1.setStoplosspips(t1.getStoplosspips());
			c1.setStopprofitpips(t2.getStoplosspips());
			c2.setStoplosspips(t1.getStoplosspips());
			c2.setStopprofitpips(t2.getStopprofitpips());
			
			c1.setType(t1.getType());
			c2.setType(t2.getType());
		} else if(rand > .625) {
			c1.setStoplosspips(t1.getStopprofitpips());
			c1.setStopprofitpips(t1.getStoplosspips());
			c2.setStoplosspips(t1.getStopprofitpips());
			c2.setStopprofitpips(t1.getStopprofitpips());
			
			c1.setType(t1.getType());
			c2.setType(t2.getType());
		} else if(rand > .5) {
			c1.setStoplosspips(t1.getStopprofitpips());
			c1.setStopprofitpips(t2.getStoplosspips());
			c2.setStoplosspips(t1.getStopprofitpips());
			c2.setStopprofitpips(t2.getStopprofitpips());
			
			c1.setType(t1.getType());
			c2.setType(t2.getType());
		} else if(rand > .375) {
			c1.setStoplosspips(t2.getStoplosspips());
			c1.setStopprofitpips(t1.getStoplosspips());
			c2.setStoplosspips(t2.getStoplosspips());
			c2.setStopprofitpips(t1.getStopprofitpips());
			
			c1.setType(t1.getType());
			c2.setType(t2.getType());
		} else if(rand > .25) {
			c1.setStoplosspips(t2.getStoplosspips());
			c1.setStopprofitpips(t2.getStoplosspips());
			c2.setStoplosspips(t2.getStoplosspips());
			c2.setStopprofitpips(t2.getStopprofitpips());
			
			c1.setType(t1.getType());
			c2.setType(t2.getType());
		} else if(rand > .175) {
			c1.setStoplosspips(t2.getStopprofitpips());
			c1.setStopprofitpips(t1.getStoplosspips());
			c2.setStoplosspips(t2.getStopprofitpips());
			c2.setStopprofitpips(t1.getStopprofitpips());
			
			c1.setType(t1.getType());
			c2.setType(t2.getType());
		} else {
			c1.setStoplosspips(t2.getStopprofitpips());
			c1.setStopprofitpips(t2.getStoplosspips());
			c2.setStoplosspips(t2.getStopprofitpips());
			c2.setStopprofitpips(t2.getStopprofitpips());
			
			c1.setType(t1.getType());
			c2.setType(t2.getType());
		}
		
		list.add(c1);
		list.add(c2);
		return list;
	}
	
	@JsonIgnore
	public void pruneTraders(Integer key) {
		ArrayList<Trader> list = map.get(key);
		
		int first = -1-000-000-000-000, second = -1-000-000-000-000;
		Trader f = null, s = null;
		while(list.size() > 0) {
			Trader t = list.remove(0);
			if(t.getPips() > first) {
				f = t;
				first = f.getPips();
			} else if(t.getPips() > second) {
				s = t;
				second = s.getPips();
			}
		}
		if(f != null) list.add(f);
		if(s != null) list.add(s);
	}
	
	@JsonIgnore
	public void pruneTraders() {
		for(Integer key: map.keySet()) {
			pruneTraders(key);
		}
	}
	
	@JsonIgnore
	private ArrayList<Trader> breedTraders(Trader t) {
		ArrayList<Trader> list = new ArrayList<Trader>();
		Trader tr = new Trader(t.getShapeId());
		if(t.getStoplosspips() > 10) {
			tr.setStoplosspips(t.getStoplosspips() + (((int)(Math.random() * 21)) - 10));
		} else {
			tr.setStoplosspips(t.getStoplosspips() + (((int)(Math.random() * 21))));
		}
		if(t.getStopprofitpips() > 10) {
			tr.setStopprofitpips(t.getStopprofitpips() + (((int)(Math.random() * 21)) - 10));
		} else {
			tr.setStopprofitpips(t.getStopprofitpips() + (((int)(Math.random() * 21))));
		}
		tr.setType(t.getType());
		
		list.add(tr);
		return list;
	}
	
	@JsonIgnore
	public void printTraderStats(Integer key) {
		ArrayList<Trader> traders = map.get(key);
		System.out.println("Traders Size: " + traders.size());
		if(traders.size() > 0) {
			int pips = 0;
			for(Trader t: traders) {
				pips += t.getPips();
			}
			System.out.println("Average Pip Size: " + (pips/traders.size()));
			System.out.println("Total Pips: " + pips);
		}
	}
	
	@JsonIgnore
	public void printTraderStats() {
		System.out.println("Shapes Size: " + map.size());
		if(map.size() > 0) {
			int pips = 0;
			ArrayList<Trader> traders = getTraderList();
			for(Trader t: traders) {
				pips += t.getPips();
			}
			double avg = 0;
			if(traders.size() > 0) {
				avg = pips / traders.size();
			}
			System.out.println("Average Pip Size: " + avg);
			System.out.println("Total Pips: " + pips);
		}
	}

	public void clearTraders() {
		for(Integer key: map.keySet()) {
			map.get(key).clear();
		}
	}
	

	public void resetTraders(Integer key) {
		resetTraders(map.get(key));
	}
	
	public void resetTraders() {
		for(Integer key: map.keySet()) {
			resetTraders(map.get(key));
		}
	}
	
	public void resetTraders(ArrayList<Trader> list) {
		for(Trader t: list) {
			t.setPips(0);
		}
	}

	public void saveTraders(String fileName) {
		File f = new File(fileName);
		if(!f.exists()) {
			try {
				FileOutputStream fos = new FileOutputStream(f);
				GZIPOutputStream gzos = new GZIPOutputStream(fos);
				ObjectOutputStream out = new ObjectOutputStream(gzos);
				out.writeObject(m.objectToJson(this));
				out.flush();
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				System.out.println("Saving Traders to file: " + fileName);
			}
		} else {
			System.out.println("Error: file already exists: " + fileName);
		}
		
	}

	public void loadTraders(String fileName) {
		File f = new File(fileName);
		if(f.exists()) {
			try {
				FileInputStream fis = new FileInputStream(f);
				GZIPInputStream gzis = new GZIPInputStream(fis);
				ObjectInputStream in = new ObjectInputStream(gzis);
				Population tp = (Population) m.jsonToObject((String)in.readObject(), Population.class);
				tp.printTraderStats();
				map = tp.getMap();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} finally {
				System.out.println("Loading Traders from file: " + fileName);
			}
		} else {
			System.out.println("Error: file does not exist: " + fileName);
		}
		
	}

}