package net.nilosplace.Data.util;

import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.Version;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.module.SimpleModule;
import org.codehaus.jackson.map.type.TypeFactory;


public class ModelMapper<T> extends ObjectMapper implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private boolean debug = false;
	
	{
		init();
	}

	public ModelMapper() { }
	
	public ModelMapper(boolean debug) {
		this.debug = debug;
	}
	
	private void init() {
		//super(new ObjectJsonFactory());
		this.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
		this.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, true);
		this.configure(SerializationConfig.Feature.WRITE_DATES_AS_TIMESTAMPS, false);
//		this.configure(Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
//		this.configure(DeserializationConfig.Feature.UNWRAP_ROOT_VALUE, true);
		
		SimpleModule module = new SimpleModule("NumberDeserializerModule", new Version(1, 0, 0, null));

		//module.addDeserializer(BigDecimal.class, new StripCommasDeserializer());
		//module.addDeserializer(Date.class, new ParseMicrosecondsDeserializer());

		registerModule(module);
	}
	
	public String objectToJson(Object object) {
		try {
			return writeValueAsString(object);
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public Object jsonToObject(String data, Class<T> c) {
		//System.out.println("Parsing: " + data);
		try {
			if(data == null || data.equals("")) {
				System.out.println("No Data to map: " + data);
				return null;
			} else {
				return readValue(data, c);
			}
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public Object jsonToObjects(String data, Object object) {
		//System.out.println("Parsing: " + data);
		try {
			TypeFactory typeFactory = TypeFactory.defaultInstance();
			readValue(data, typeFactory.constructCollectionType(ArrayList.class, object.getClass()));
			return readValue(data, object.getClass());
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
