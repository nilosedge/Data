package net.nilosplace.Data.models;

import java.math.BigDecimal;


public class Order {
	
	private BigDecimal amount = BigDecimal.ZERO;
	private Type type = Type.HOLD;
	private int stoplosspips = 0;
	private int stopprofitpips = 0;

	public Order() {}
	
	public Order(Order order) {
		this.amount = order.getAmount();
		this.type = order.getType();
		this.stoplosspips = order.getStoplosspips();
		this.stopprofitpips = order.getStopprofitpips();
	}

	public Order(Type type, int stopprofitpips, int stoplosspips) {
		this.type = type;
		this.stoplosspips = stoplosspips;
		this.stopprofitpips = stopprofitpips;
	}

	public enum Type {
		BUY, SELL, HOLD;
	}
	
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public Type getType() {
		return type;
	}
	public void setType(Type type) {
		this.type = type;
	}
	public int getStoplosspips() {
		return stoplosspips;
	}
	public void setStoplosspips(int stoplosspips) {
		this.stoplosspips = stoplosspips;
	}
	public int getStopprofitpips() {
		return stopprofitpips;
	}
	public void setStopprofitpips(int stopprofitpips) {
		this.stopprofitpips = stopprofitpips;
	}
}
