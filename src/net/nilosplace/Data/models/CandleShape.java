package net.nilosplace.Data.models;

import org.codehaus.jackson.annotate.JsonIgnore;


public class CandleShape {

	public int open = 0;
	public int close = 0;
	public int pips = 0;
	
	public String id = "";
	public int idInt = 0;

	public CandleShape() {}
	public CandleShape(int open, int close, int pips) {
		this.open = open;
		this.close = close;
		this.pips = pips;
		this.id = open + "-" + close + "-" + pips;
		this.idInt = (open << 20) | (close << 10) | pips;
	}


	@JsonIgnore
	public String getId() {
		if(id.length() == 0) {
			id = open + "-" + close + "-" + pips;
			idInt = (open << 20) | (close << 10) | pips;
		}
		return id;
	}
	@JsonIgnore
	public int getIdInt() {
		if(id.length() == 0) {
			id = open + "-" + close + "-" + pips;
			idInt = (open << 20) | (close << 10) | pips;
		}
		return idInt;
	}

	public int getOpen() {
		return open;
	}
	public void setOpen(int open) {
		this.open = open;
	}
	public int getClose() {
		return close;
	}
	public void setClose(int close) {
		this.close = close;
	}
	public int getPips() {
		return pips;
	}
	public void setPips(int pips) {
		this.pips = pips;
	}
}
