package net.nilosplace.Data.models;

import org.codehaus.jackson.annotate.JsonIgnore;

import net.nilosplace.Data.models.Order.Type;


public class Trader {

	//private CandleShape candle = new CandleShape();
	private int pips = 0;
	private int shapeId = 0;
	
	private Type type;
	private int stoplosspips = 0;
	private int stopprofitpips = 0;
	
	private boolean debug = false;
	private static int traderIdCounter = 0;
	private int traderId = 0;

	{
		traderId = Trader.traderIdCounter++;
		if(Math.random() > .5) {
			type = Type.BUY;
		} else {
			type = Type.SELL;
		}
		stoplosspips = (int) ((Math.random() * 150) + 5);
		stopprofitpips = (int) ((Math.random() * 150) + 5);
	}
	
	public Trader() {}
	
	public Trader(int shapeId) {
		this.shapeId = shapeId;
	}
	
	@JsonIgnore
	public Order getOrder() {
		return new Order(type, stopprofitpips, stoplosspips);
	}
	

	public void profit(int pips) {
		this.pips += pips;
	}
	public void loss(int pips) {
		this.pips -= pips;
	}
	public int getShapeId() {
		return shapeId;
	}
	public void setShapeId(int shapeId) {
		this.shapeId = shapeId;
	}
	public boolean isDebug() {
		return debug;
	}
	public void setDebug(boolean debug) {
		this.debug = debug;
	}
	public int getTraderId() {
		return traderId;
	}
	public void setTraderId(int traderId) {
		this.traderId = traderId;
	}
	public int getPips() {
		return pips;
	}
	public void setPips(int pips) {
		this.pips = pips;
	}
	public Type getType() {
		return type;
	}
	public void setType(Type type) {
		this.type = type;
	}
	public int getStoplosspips() {
		return stoplosspips;
	}
	public void setStoplosspips(int stoplosspips) {
		this.stoplosspips = stoplosspips;
	}
	public int getStopprofitpips() {
		return stopprofitpips;
	}
	public void setStopprofitpips(int stopprofitpips) {
		this.stopprofitpips = stopprofitpips;
	}
}
