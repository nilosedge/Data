package net.nilosplace.Data.models;


public class Candle {
	
	double open = 0;
	double high = 0;
	double low = 0;
	double close = 0;
	double size = 0;
	int sizepips = 0;
	CandleShape shape;

	public Candle(double open, double high, double low, double close) {
		this.open = open;
		this.high = high;
		this.low = low;
		this.close = close;
		
		size = high - low;
		sizepips = (int)(size * 100);
		
		if(size > 0) {
			shape = new CandleShape(
				(int)(((open - low) * 100) / size), // open.subtract(low).multiply(BigDecimal.valueOf(100)).divide(size, 0, RoundingMode.HALF_UP).intValue(),
				(int)(((close - low) * 100) / size),  //close.subtract(low).multiply(BigDecimal.valueOf(100)).divide(size, 0, RoundingMode.HALF_UP).intValue(),
				sizepips
			);
		} else {
			shape = new CandleShape(0, 0, 0);
		}
		
//		System.out.println("Open: " + open);
//		System.out.println("High: " + high);
//		System.out.println("Low: " + low);
//		System.out.println("Close: " + close);
//		System.out.println("Size: " + size);  
		
		//writer.write("Candle: Open: " + open + "% Close: " + close + "% Pips: " + sizepips + "\n");
		//System.out.println("Candle: Open: " + open + "% Close: " + close + "% Pips: " + sizepips);
		//System.out.println(sizepips);
		
		//System.out.println();
	}


	public double getOpen() {
		return open;
	}
	public void setOpen(double open) {
		this.open = open;
	}
	public double getHigh() {
		return high;
	}
	public void setHigh(double high) {
		this.high = high;
	}
	public double getLow() {
		return low;
	}
	public void setLow(double low) {
		this.low = low;
	}
	public double getClose() {
		return close;
	}
	public void setClose(double close) {
		this.close = close;
	}
	public double getSize() {
		return size;
	}
	public void setSize(double size) {
		this.size = size;
	}
	public int getSizepips() {
		return sizepips;
	}
	public void setSizepips(int sizepips) {
		this.sizepips = sizepips;
	}
	public CandleShape getShape() {
		return shape;
	}
	public void setShape(CandleShape shape) {
		this.shape = shape;
	}
}